# Generates an image with https://github.com/lumeohq/onvif-rs ready-to-go.


# Using rust_dev_env as the base image.
FROM alpacabalena/rust_dev_env:2022-04-09_184924
# Specifying the shell for all the following RUN commands.
SHELL ["/bin/bash", "-c"]
# Let's get the lumeohq/onvif-rs code and build it.
RUN git clone https://github.com/lumeohq/onvif-rs.git
WORKDIR /onvif-rs
RUN source $HOME/.cargo/env && cargo build && cargo build --example discovery && cargo build --example camera
# At this point, ```docker images``` and find the id of your image. Then, ```docker run --name rustdev01 -d IMAGE_ID sleep infinity```.
# Then, ```docker exec -it rustdev01 bash```. Now, you can try everything you want in that container.
# Remember that what happens in the container stays in the container! When it shuts down, you'll lose all changes. Very practical to experiment.

